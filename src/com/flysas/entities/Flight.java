package com.flysas.entities;

public class Flight {
	private String depTime;// should be LocalDateTime
	private String depAirport;
	private String arrTime;// should be LocalDateTime
	private String arrAirport;
	private String connAirport;
	private Double cheapestPrice;

	public Flight(String depTime, String arrTime, Double cheapestPrice) {
		this.depTime = depTime;
		this.arrTime = arrTime;
		this.cheapestPrice = cheapestPrice;
	}

	public String getDepTime() {
		return depTime;
	}

	public void setDepTime(String depTime) {
		this.depTime = depTime;
	}

	public String getDepAirport() {
		return depAirport;
	}

	public void setDepAirport(String depAirport) {
		this.depAirport = depAirport;
	}

	public String getArrTime() {
		return arrTime;
	}

	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}

	public String getArrAirport() {
		return arrAirport;
	}

	public void setArrAirport(String arrAirport) {
		this.arrAirport = arrAirport;
	}

	public String getConnAirport() {
		return connAirport;
	}

	public void setConnAirport(String connAirport) {
		this.connAirport = connAirport;
	}

	public Double getCheapestPrice() {
		return cheapestPrice;
	}

	public void setCheapestPrice(Double cheapestPrice) {
		this.cheapestPrice = cheapestPrice;
	}

	@Override
	public String toString() {
		return "Flight [depTime=" + depTime + ", depAirport=" + depAirport
				+ ", arrTime=" + arrTime + ", arrAirport=" + arrAirport
				+ ", connAirport=" + connAirport + ", cheapestPrice="
				+ cheapestPrice + "]";
	}
}
