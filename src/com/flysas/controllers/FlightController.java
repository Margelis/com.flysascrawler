package com.flysas.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.flysas.entities.Flight;

public class FlightController {
	
	public void printDirectFlightsAndConnectedTo(String connectedAirport) {
		Document doc = null;
		try {
			doc = Jsoup.connect("myPageUrl").get();
		} catch (IOException e) {
			System.err.println("An error occurs while connecting to source: "
					+ e.getMessage());
		}
		// selects departure and return tables
		Elements tablesElements = doc.select("#outer #inner #flow");
		
		Elements departureTable = tablesElements
				.select("#flow .frame .outbound #panel_0 table tbody");
		Elements returnTable = tablesElements
				.select("#flow .frame .outbound #panel_1 table tbody");

		List<Flight> depList = findDirectAndConnectedFlightsFor(departureTable,
				connectedAirport);
		List<Flight> retList = findDirectAndConnectedFlightsFor(returnTable,
				connectedAirport);

		for (Flight f : depList) {
			System.out.println(f.toString());
		}

		for (Flight f : retList) {
			System.out.println(f.toString());
		}
	}

	public List<Flight> findDirectAndConnectedFlightsFor(Elements tableElement,
			String connectionAirport) {
		List<Flight> flightsList = new ArrayList<Flight>();
		Flight currentFlight = null;
		int trCounter = 0;

		for (Element tr : tableElement.select("tbody tr")) {
			if (trCounter == 0) {
				currentFlight = createNewFlight(tr);
				trCounter++;
			} else if (trCounter == 1) {
				currentFlight = generateFlightsData(connectionAirport,
						flightsList, currentFlight, tr);
				trCounter++;
			} else {
				trCounter = 0;
				currentFlight = null;
			}
		}
		return flightsList;
	}

	private Flight generateFlightsData(String connectionAirport,
			List<Flight> flightsList, Flight currentFlight, Element tr) {
		String depLocationSelector = "table tbody tr:nth-child(2) td[class=\"route\"] span:first-child .location";
		String arrLocationSelector = "table tbody tr:nth-child(2) td[class=\"route\"] span:last-child .location";
		String depAirport = tr.select(depLocationSelector).text()
				+ tr.select(depLocationSelector + " .airport").text();// Stockholm, Arlanda

		if (tr.select("table tbody tr").size() == 4) {// direct flights
			setDataForDirectFlight(currentFlight, tr,
					arrLocationSelector, depAirport);
			flightsList.add(currentFlight);

		} else if (tr.select("table tbody tr").size() == 8) {// non direct flights
			String connAirport = tr.select(arrLocationSelector).text()
					+ tr.select(arrLocationSelector + " .airport").text();
			String arrLocSelector = "table tbody tr:nth-child(5) td[class=\"route\"] span:last-child .location";
			String arrAirport = tr.select(arrLocSelector).text()
					+ tr.select(arrLocSelector + " .airport").text();

			if (connAirport.toLowerCase().contains(
					connectionAirport.toLowerCase())) {// check if connection airport are the same
				setDataForConnectedAirport(currentFlight, depAirport,
						connAirport, arrAirport);
				flightsList.add(currentFlight);
			} else {
				currentFlight = null;
			}
		}
		return currentFlight;
	}

	private void setDataForConnectedAirport(Flight currentFlight,
			String depAirport, String connAirport, String arrAirport) {
		currentFlight.setDepAirport(depAirport);
		currentFlight.setConnAirport(connAirport);
		currentFlight.setArrAirport(arrAirport);
	}

	private void setDataForDirectFlight(Flight currentFlight, Element tr,
			String arrLocationSelector, String depAirport) {
		String arrAirport = tr.select(arrLocationSelector).text()
				+ tr.select(arrLocationSelector + " .airport").text();
		currentFlight.setArrAirport(arrAirport);
		currentFlight.setDepAirport(depAirport);
	}

	private Flight createNewFlight(Element tr) {
		Double cheapestPrice = Double.valueOf(tr.select("td").first()
				.select(".choise .price .number").text());
		String depTime = tr.select("td[class=\"time\"] span:first-child")
				.text();
		String arrTime = tr.select("td[class=\"time\"] span:last-child").text();

		return new Flight(depTime, arrTime, cheapestPrice);
	}
}
